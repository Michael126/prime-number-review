package sheridan;

public class Prime {
	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Prime.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + Prime.isPrime( 36 ) );		
		System.out.println( "Number 234 is prime? " + Prime.isPrime( 234 ) );	//By: Michael Kornecki
		System.out.println( "Number 2344 is prime? " + Prime.isPrime( 2344 ) );	//By: Michael Kornecki 2nd branch
		System.out.println( "Number 99 is prime? " + Prime.isPrime( 99 ) );	//Change by: Ryan Warnakula
		System.out.println( "Number 999 is prime? " + Prime.isPrime( 999 ) );	//Change by: Ryan Warnakula 2nd branch
	}

}
